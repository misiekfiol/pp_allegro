<%@page import="javax.print.DocFlavor.URL"%>
<%@page import="classes.Users.Address"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.ArrayList"%>
<%@page import="classes.Product.Auction"%>
<%@page import="com.sun.org.apache.xerces.internal.impl.dv.util.Base64"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="java.sql.Blob"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.File"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%-- 
    Document   : auction
    Author     : Mikołaj Sikorski
--%>

<html>
    <head><title>Auction Descryption</title></head>
    <body>
        <%-- Połączenie z baza --%>
        <%
            java.sql.Connection con = null;
            java.sql.Statement stmt = null;
            java.sql.ResultSet rs = null;
            java.sql.PreparedStatement ps = null;

            String sqlUrl = "jdbc:derby://localhost:1527/BazaDanych";
            String sqlShema = "baza";
            String sqlUser = "baza";
            
            String title = "";
            double price = 0;
            String descryption = "";
            Date expirationDate = null;
            Blob photo = null;
            int address_id = 0;
            int end_id = 0;
            int owner_id = 0;

            String id_auction = request.getParameter("id_auction");
            String url = "";
            Blob blob;

            try {
                Class.forName("org.apache.derby.jdbc.ClientDriver");
                con = java.sql.DriverManager.getConnection(sqlUrl, sqlShema, sqlUser);
        %>
        <%-- Zapytanie wysylane do bazy --%>
        <%
            String sqlString = "select * from auction where id_auction = " + id_auction;

            stmt = con.createStatement();
            rs = stmt.executeQuery(sqlString);
        %>
        <%-- Generowanie tabeli --%> 
        <%
            while (rs.next()) 
            {
                title = rs.getString("title");
                price = rs.getDouble("price");
                descryption = rs.getString("descryption");
                expirationDate = rs.getDate("expirationDate");
                photo = rs.getBlob("photo");
                address_id = rs.getInt("address_id");
                end_id = rs.getInt("end_id");
                owner_id = rs.getInt("owner_id");
                
                if (photo != null)
                {
                    byte[] image = photo.getBytes(1, (int) photo.length());
                    url = "data:image/png;base64," + Base64.encode(image);
                //    url = URL.createObjectURL(blob);
                }
                else 
                {
                    url = "http://www.mavent.pl/images/produkty_shop/no_foto.png";
                } 
            }

            %>
            <center>
            <table height="100%" width="100%">
            <%        
                
            %>            
  <tr>
    <th colspan="3"><img src="<%=url%>"></th>
  </tr>
  <tr>
    <td></td>
    <td>Title: <%=title%></td>
    <td></td>
  </tr>
  <tr>
    <td>Price: <%=price%></td>
    <td></td>
    <td>Expiration Date: <%=expirationDate%></td>
  </tr>
  <tr>
    <td colspan="3"><%=descryption%></td>
  </tr>
  <tr>
    <td><a href="index.htm"> <-Back </td>
    <td><form method="post" action="index.htm">
            <% con = java.sql.DriverManager.getConnection(sqlUrl, sqlShema, sqlUser);
            sqlString = "DELETE FROM auction WHERE id_auction = " + id_auction;

            stmt = con.createStatement();
            stmt.executeUpdate(sqlString);
            
            %>
            <input type="submit" value="BUY" name="buy" />
        </form>
    </td>
    <td>Owner: <%=owner_id%></td>
  </tr>
          
          
          
          
            </table>
  </center>
            <%
        %>
        <%-- Lapanie wyjatkow i zamykanie polaczen --%>   
        <%        }
        catch (Exception ex) {
        %><%=ex%><%
                        ex.printStackTrace();
                    }
                    finally {
                        try {
                            if (rs != null) {
                                rs.close();
                                rs = null;
                            }
                            if (ps != null) {
                                ps.close();
                                ps = null;
                            }
                            if (con != null) {
                                con.close();
                                con = null;
                            }
                        }
                        catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
        %>
    </body>
</html>
