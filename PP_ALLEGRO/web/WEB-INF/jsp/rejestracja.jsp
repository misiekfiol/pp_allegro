<%-- 
    Document   : rejestracja
    Author     : Michał Tworuszka
--%>

<%@page import="classes.Users.Address"%>
<%@page import="classes.Users.User"%>
<%@page import="org.hibernate.Transaction"%>
<%@page import="hiberApp.HiberUtil"%>
<%@page import="org.hibernate.SessionFactory"%>
<%@page import="org.hibernate.Session"%>
<%@page import="classes.Users.Owasp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Rejestracja</title>
    </head>
    <body>
        <%
            Owasp addUser = new Owasp();
            java.sql.Connection con = null;
            java.sql.ResultSet rs = null;
            java.sql.PreparedStatement ps = null;
            String password = null;
            String passwordCheck = null;

            String sqlUrl = "jdbc:derby://localhost:1527/BazaDanych";
            String sqlShema = "baza";
            String sqlUser = "baza";

            try {
                Class.forName("org.apache.derby.jdbc.ClientDriver");
                con = java.sql.DriverManager.getConnection(sqlUrl, sqlShema, sqlUser);
        %>


        <form action="rejestracja.htm" method="POST">

            <%/*
                 <br/>Login (mail):<input type="email" name="login">
                 <br/>Hasło:<input type="password" name="pass">
                 <br/>powtórz hasło:<input type="password" name="passCheck">
                 <br/>
                 <br/>Imie:<input type="text" name="first_name">
                 <br/>Nazwisko:<input type="text" name="last_name">
                 <br/>Podaj Adres
                 <br/>Kraj:<input type="text" name="country">
                 <br/>Miasto:<input type="text" value="" name="city">
                 <br/>Kod pocztowy<input type="text" name="postalCode">
                 <br/>Ulica<input type="text" name="street">
                 <br/>nr domu<input type="text" name="house">
                 <br/>nr mieszkania<input type="text" name="flat">

                 <br/><input type="submit" value="Rejestrój" name="register">
                 */%>

            <table border="0" width="20%">
                <tbody>
                    <tr>
                        <td><br/>Login (mail):</td>
                        <td><input type="email" name="login"></td>
                    </tr>
                    <tr>
                        <td><br/>Hasło:</td>
                        <td><input type="password" name="pass" value=""></td>
                    </tr>
                    <tr>
                        <td><br/>powtórz hasło:</td>
                        <td><input type="password" name="passCheck" value=""></td>
                    </tr>
                    <tr>
                        <td><br/>Imie:</td>
                        <td><input type="text" name="first_name"></td>
                    </tr>
                    <tr>
                        <td><br/>Nazwisko:</td>
                        <td><input type="text" name="last_name"></td>
                    </tr>
                    <tr>
                        <td><br/>Podaj Adres</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><br/>Kraj:</td>
                        <td><input type="text" name="country"></td>
                    </tr>
                    <tr>
                        <td><br/>Miasto:</td>
                        <td><input type="text" value="" name="city"></td>
                    </tr>
                    <tr>
                        <td><br/>Kod pocztowy</td>
                        <td><input type="text" name="POSTAL_CODE"></td>
                    </tr>
                    <tr>
                        <td><br/>Ulica</td>
                        <td><input type="text" name="street"></td>
                    </tr>
                    <tr>
                        <td><br/>Nr domu</td>
                        <td><input type="text" name="house"></td>
                    </tr>
                    <tr>
                        <td><br/>Nr mieszkania</td>
                        <td><input type="text" name="flat"></td>
                    </tr>
                    <tr>
                        <td><br/><input type="submit" value="Rejestruj" name="register"></td>
                        <td>
                            <%
                                    String login = request.getParameter("login");
                                    password = request.getParameter("pass").toString();
                                    passwordCheck = request.getParameter("passCheck").toString();
                                    String salt = null;

                                    ps = con.prepareStatement("SELECT LOGIN FROM USER_TABLE WHERE LOGIN = '" + login + "'");
                                    //ps.setString(1, login);
                                    rs = ps.executeQuery();
                                    if (!rs.next()) {

                                        if (password.equals("")) {
                                            out.println("Brak haseł");
                                        }
                                        else {
                                            if (password.equals(passwordCheck)) {

                                                addUser.createUser(con, login, password);
                                                out.println("Zostałeś zarejestrowany");
                                                Address address = new Address(request.getParameter("country").toString(),
                                                        request.getParameter("city").toString(),
                                                        request.getParameter("POSTAL_CODE").toString(),
                                                        request.getParameter("street").toString(),
                                                        request.getParameter("house").toString(),
                                                        request.getParameter("flat").toString());

                                                ps = con.prepareStatement("INSERT INTO ADDRESS (country, city, POSTAL_CODE, street, house, flat) values (?,?,?,?,?,?)");
                                                ps.setString(1, address.getCountry());
                                                ps.setString(2, address.getCity());
                                                ps.setString(3, address.getPostalCode());
                                                ps.setString(4, address.getStreet());
                                                ps.setString(5, address.getHouse());
                                                ps.setString(6, address.getFlat());
                                                ps.executeUpdate();

                                                ps = con.prepareStatement("SELECT MAX(ID_ADDRESS) as ID_ADDRESS from ADDRESS");
                                                rs = ps.executeQuery();
                                                if (rs.next()) {
                                                    address.setId_address(rs.getInt("ID_ADDRESS"));
                                                }

                                                ps = con.prepareStatement("SELECT SALT FROM USER_TABLE WHERE LOGIN = '" + login + "'");
                                                rs = ps.executeQuery();
                                                if (rs.next()) {
                                                    salt = rs.getString("SALT");
                                                }

                                                User user = new User(login, password, salt,
                                                        request.getParameter("first_name"),
                                                        request.getParameter("last_name"), false, address.getId_address());
                                                addUser.authenticate(con, login, password);

                                                ps = con.prepareStatement("Update USER_TABLE set first_name = ?, last_name = ?, is_admin = ?, address_id = ? where LOGIN = ?");
                                                ps.setString(1, user.getFirst_name());
                                                ps.setString(2, user.getLast_name());
                                                ps.setBoolean(3, user.isIsAdmin());
                                                ps.setInt(4, user.getAddress_id());
                                                ps.setString(5, login);
                                                ps.executeUpdate();

                                                Cookie cookieLogin = new Cookie("login", login);
                                                Cookie cookiePass = new Cookie("pass", password);
                                                response.addCookie(cookieLogin);
                                                response.addCookie(cookiePass);
                                            }
                                            else {
                                                out.println("hasła nie są identyczne");

                                            }
                                        }
                                    }

                                    else {
                                        out.println("istnieje już taki login - zarejestruj się przy pomocy innego maila");
                                        
                                    }
                                }

                                catch (Exception ex) {
                                    ex.printStackTrace();
                                    //out.println(ex);
                                }
                                finally {
                                    try {
                                        if (rs != null) {
                                            rs.close();
                                            rs = null;
                                        }
                                        if (ps != null) {
                                            ps.close();
                                            ps = null;
                                        }
                                        if (con != null) {
                                            con.close();
                                            con = null;
                                        }
                                    }
                                    catch (Exception ex) {
                                        ex.printStackTrace();
                                       // out.println(ex);
                                    }
                                }
                            %>
                        </td>
                    </tr>
                </tbody>
            </table>

        </form>

        <form action="index.htm" method="POST">
            <input type="submit" name="BACK" value="Powrót do Strony głownej">
        </form>
    </body>
</html>
