<%-- 
    Document   : zarzadzajUserem
    Author     : Michał Tworuszka
--%>

<%@page import="classes.Users.Address"%>
<%@page import="classes.Users.User"%>
<%@page import="classes.Users.Owasp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Zarządzanie danymi użytkownika</title>
    </head>
    <body>
        <form action="zarzadzajUserem.htm" method="POST">
            <%
                Owasp addUser = new Owasp();
                User user = new User();
                Address address = new Address();
                java.sql.Connection con = null;
                java.sql.ResultSet rs = null;
                java.sql.PreparedStatement ps = null;
                String password = null;
                String passwordCheck = null;

                String sqlUrl = "jdbc:derby://localhost:1527/BazaDanych";
                String sqlShema = "baza";
                String sqlUser = "baza";

                try {
                    Class.forName("org.apache.derby.jdbc.ClientDriver");
                    con = java.sql.DriverManager.getConnection(sqlUrl, sqlShema, sqlUser);

                    Cookie cookie = null;
                    Cookie[] cookies = null;
                    boolean exist = false;
                    // Get an array of Cookies associated with this domain
                    cookies = request.getCookies();
                    if (cookies != null) {
                        Owasp passCheck = new Owasp();

                        Class.forName("org.apache.derby.jdbc.ClientDriver");
                        con = java.sql.DriverManager.getConnection(sqlUrl, sqlShema, sqlUser);
                        String salt = null;
                        String login = null;

                        for (int i = 0; i < cookies.length; i++) {
                            cookie = cookies[i];
                            if (cookie.getName().equals("login")) {
                                login = cookie.getValue();

                            }
                            else if (cookie.getName().equals("pass")) {
                                password = cookie.getValue();
                            }
                        }
                        exist = passCheck.authenticate(con, login, password);
                        if (exist == true) {
                            user.setLogin(login);
                            user.setPassword(password);

                            ps = con.prepareStatement("SELECT FIRST_NAME, LAST_NAME, ADDRESS_ID FROM USER_TABLE where login ='" + login + "'");
                            rs = ps.executeQuery();
                            if (rs.next()) {
                                user.setFirst_name(rs.getString("FIRST_NAME"));
                                user.setLast_name(rs.getString("LAST_NAME"));
                                user.setAddress_id(rs.getInt("ADDRESS_ID"));
                            }
                            ps = con.prepareStatement("select * from address where id_address = " + user.getAddress_id());
                            rs = ps.executeQuery();
                            if (rs.next()) {
                                address = new Address(user.getAddress_id(),
                                        rs.getString("country"),
                                        rs.getString("city"),
                                        rs.getString("POSTAL_CODE"),
                                        rs.getString("street"),
                                        rs.getString("house"),
                                        rs.getString("flat"));
                            }
                        }
                    }
            %>
            <form action="zarzadzajUserem.htm" method="POST">
                <table border="0" width="20%" >
                    <thead>
                        <tr>
                            <th>Opis</th>
                            <th>Twoje dane</th>
                            <th>Wybórpola zmiany</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Imie: </td>
                            <td> <% out.print(user.getFirst_name()); %></td>
                            <td><input type="checkbox" name="check" value="Imie" /> </td>
                        </tr>
                        <tr>
                            <td>Nazwisko: </td>
                            <td><% out.print(user.getLast_name()); %></td>
                            <td><input type="checkbox" name="check" value="Nazwisko" /> </td>
                        </tr>
                        <tr>
                            <td>Kraj: </td>
                            <td><% out.print(address.getCountry()); %> </td>
                            <td><input type="checkbox" name="check" value="Kraj" /> </td>
                        </tr>
                        <tr>
                            <td>Miasto: </td>
                            <td><% out.print(address.getCity()); %></td>
                            <td><input type="checkbox" name="check" value="Miasto" /> </td>
                        </tr>
                        <tr>
                            <td>Kod Pocztowy: </td>
                            <td><% out.print(address.getPostalCode()); %></td>
                            <td><input type="checkbox" name="check" value="KodPocztowy" /> </td>
                        </tr>
                        <tr>
                            <td>Ulica: </td>
                            <td><% out.print(address.getStreet()); %></td>
                            <td><input type="checkbox" name="check" value="Ulica" /> </td>
                        </tr>
                        <tr>
                            <td>Dom: </td>
                            <td><% out.print(address.getHouse()); %></td>
                            <td><input type="checkbox" name="check" value="Dom" /> </td>
                        </tr>
                        <tr>
                            <td>Mieszkanie: </td>
                            <td><% out.print(address.getFlat()); %></td>
                            <td><input type="checkbox" name="check" value="Mieszkanie" /> </td>
                        </tr>
                    </tbody>
                </table>
                <br/>
                <input type="submit" value="submit"/>
            </form>
            <%
                    String checks[] = request.getParameterValues("check");
                    if (checks != null) {
                        for (int i = 0; i < checks.length; ++i) {
                            out.print(checks[i] + " ");
                        }
                    }
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                    out.println(ex);
                }
                finally {
                    try {
                        if (rs != null) {
                            rs.close();
                            rs = null;
                        }
                        if (ps != null) {
                            ps.close();
                            ps = null;
                        }
                        if (con != null) {
                            con.close();
                            con = null;
                        }
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                        out.println(ex);
                    }
                }
            %> 
            <form action="index.htm" method="POST">
                <input type="submit" name="BACK" value="Powrót do Strony głownej">
            </form>
    </body>
</html>
