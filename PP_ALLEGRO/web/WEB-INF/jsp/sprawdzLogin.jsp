<%-- 
    Document   : SprawdzLogin
    Author     : Michał Tworuszka
--%>

<%@page import="java.sql.ResultSet"%>
 <%@page import="classes.Users.Owasp"%>
 <%@page contentType="text/html" pageEncoding="UTF-8"%>
 <html>
     <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <title>JSP Page</title>
     </head>
     <body>
         <%
             Owasp passCheck = new Owasp();
             boolean exist = false;
             java.sql.Connection con = null;
             java.sql.ResultSet rs = null;
             java.sql.PreparedStatement ps = null;
 
             String sqlUrl = "jdbc:derby://localhost:1527/BazaDanych";
             String sqlShema = "baza";
             String sqlUser = "baza";
 
             String login = request.getParameter("login");
             String password = request.getParameter("pass");
 
             try {
                 Class.forName("org.apache.derby.jdbc.ClientDriver");
                 con = java.sql.DriverManager.getConnection(sqlUrl, sqlShema, sqlUser);
                 String salt = null;
 
                 exist = passCheck.authenticate(con, login, password);
 
                 if (exist == true) {
                     out.println("Zostaleś zalogowany");
                     Cookie cookieLogin = new Cookie("login", login);
                     Cookie cookiePass = new Cookie("pass", password);
                     response.addCookie(cookieLogin);
                     response.addCookie(cookiePass);
                     %>
         <form action="index.htm" method="POST">
             <input type="submit" name="BACK" value="Powrót do Strony głównej">
         </form>
         <%
         }
         else {
             out.println("Nie ma takiego loginu");
             Cookie cookieLogin = new Cookie("login", null);
             Cookie cookiePass = new Cookie("pass", null);
             response.addCookie(cookieLogin);
             response.addCookie(cookiePass);
         %>
         <form action="rejestracja.htm" method="POST">
             <input type="submit" name="BACK" value="Zarejestruj się">
         </form>
         <%
                 }
             }
             catch (Exception ex) {
                 ex.printStackTrace();
             }
             finally {
                 try {
                     if (rs != null) {
                         rs.close();
                         rs = null;
                     }
                     if (ps != null) {
                         ps.close();
                         ps = null;
                     }
                     if (con != null) {
                         con.close();
                         con = null;
                     }
                 }
                 catch (Exception ex) {
                     ex.printStackTrace();
                 }
             }
         %>
     </body>
 </html>