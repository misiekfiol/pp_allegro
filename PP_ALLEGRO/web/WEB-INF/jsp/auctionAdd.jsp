<%@page import="java.sql.Date"%>
<%@page import="java.sql.Blob"%>
<%@page import="java.util.Random"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.File"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%-- 
    Document   : dodawanie
    Author     : Mikołaj Sikorski
--%>

<head><title>Dodawanie</title></head>
<h1>Inserting an Auction into Database</h1>
<%
    java.sql.Connection con = null;
    java.sql.ResultSet rs = null;
    java.sql.PreparedStatement ps = null;

    String sqlUrl = "jdbc:derby://localhost:1527/BazaDanych";
    String sqlShema = "baza";
    String sqlUser = "baza";

    Random generator = new Random();
    int rand = generator.nextInt(1000) + 1;

    try {
        Class.forName("org.apache.derby.jdbc.ClientDriver");
        con = java.sql.DriverManager.getConnection(sqlUrl, sqlShema, sqlUser);
        //File img = new File("D://Gallery/44463_26.jpg");
        //FileInputStream fis = new FileInputStream(img);
        //int imglength = (int)img.length();
        
        %>
<form method="post" action="index.htm">
        <table border="0" width="100%">
                <tbody>
                    <tr>
                        <td><br/>price:</td>
                        <td><input type="number" name="price"/></td>
                    </tr>
                    <tr>
                        <td><br/>title: </td>
                        <td><input type="text" name="title"/></td>
                    </tr>
                    <tr>
                        <td><br/>descryption: </td>
                        <td><input type="text" name="descryption"/></td>
                    </tr>
                    <tr>
                        <td><br/>expirationDate: </td>
                        <td><input type="date" name="expirationDate"/></td>
                    </tr>
                    <tr>
                        <td><br/>photo: </td>
                        <td><input type="file" name="photo" accept="image/*"/></td>
                    </tr>
                    <tr>
                        <td><br/><input type="submit" value="Insert" name="insert"></td>
                    </tr>
                       </tbody>
            </table>

    
                 <%
        String price = request.getParameter("price").toString();
        String title = request.getParameter("title").toString();
        String descryption = request.getParameter("descryption").toString();
        Date expirationDate = (Date) request.getAttribute("expirationDate");
        InputStream  photo = (InputStream) request.getAttribute("photo");
        %>
</form>
             <%
   
                ps = con.prepareStatement("insert into auction(price, title, descryption, expirationDate, photo)" + "values(?,?,?,?,?)");
                //ps = con.prepareStatement("insert into auction(price, title, descryption) " + "values(?,?,?)");
                ps.setString(1, price);
                ps.setString(2, title); 
                ps.setString(3, descryption);
                ps.setDate(4, expirationDate);
                ps.setBlob(5, photo);
                ps.executeUpdate();

}
catch (Exception ex) {
//out.println(ex);
ex.printStackTrace();
}
finally {
try {
    if (rs != null) {
        rs.close();
        rs = null;
    }
    if (ps != null) {
        ps.close();
        ps = null;
    }
    if (con != null) {
        con.close();
        con = null;
    }
}
catch (Exception ex) {
out.println(ex);
            ex.printStackTrace();
        }
    }

%>
</body>
