<%@page import="javax.print.DocFlavor.URL"%>
<%@page import="classes.Users.Address"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.ArrayList"%>
<%@page import="classes.Product.Auction"%>
<%@page import="com.sun.org.apache.xerces.internal.impl.dv.util.Base64"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="java.sql.Blob"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.File"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%-- 
    Document   : wypisywanie
    Author     : Mikołaj Sikorski, Michał Tworuszka
--%>

<% //Przykład pobierania bazy danych ze sronki: http://www.java-samples.com/showtutorial.php?tutorialid=619 %>
<html>
    <head><title>Showing</title></head>
    <body>
        <%-- Połączenie z baza --%>
        <%
            java.sql.Connection con = null;
            java.sql.Statement stmt = null;
            java.sql.ResultSet rs = null;
            java.sql.PreparedStatement ps = null;

            String sqlUrl = "jdbc:derby://localhost:1527/BazaDanych";
            String sqlShema = "baza";
            String sqlUser = "baza";
            
            int id_auction = 0;
            String title = "";
            double price = 0;
            String descryption = "";
            Date expirationDate = null;
            Blob photo = null;
            int address_id = 0;
            int end_id = 0;
            int owner_id = 0;

            String url = "";
            ArrayList<Auction> auction = new ArrayList<Auction>();
            ArrayList<String> urlList = new ArrayList<String>();
            int listMax = 30;

            try {
                Class.forName("org.apache.derby.jdbc.ClientDriver");
                con = java.sql.DriverManager.getConnection(sqlUrl, sqlShema, sqlUser);
        %>
        <%-- Zapytanie wysylane do bazy --%>
        <%
            String sqlString = "select * from auction";

            stmt = con.createStatement();
            rs = stmt.executeQuery(sqlString);
        %>
        <%-- Generowanie tabeli --%> 
        <%
            while (rs.next()) 
            {
                id_auction = rs.getInt("id_auction");
                title = rs.getString("title");
                price = rs.getDouble("price");
                descryption = rs.getString("descryption");
                expirationDate = rs.getDate("expirationDate");
                photo = rs.getBlob("photo");
                address_id = rs.getInt("address_id");
                end_id = rs.getInt("end_id");
                owner_id = rs.getInt("owner_id");
                
                if (photo != null)
                {
                   byte[] image = photo.getBytes(1, (int) photo.length());
                   url = "data:image/png;base64," + Base64.encode(image);
                }
                else 
                {
                    url = "http://www.mavent.pl/images/produkty_shop/no_foto.png";
                } 
                
                
                urlList.add(url);
                auction.add(new Auction(id_auction, title, price, descryption, expirationDate, photo, address_id, end_id, owner_id));
            }

            %><table align="center" width="100%" rules="rows" frame="hsides">
            <%
            for (int i = 1; i < auction.size(); i=i+1)
            {         
                
                //if (listMax == 0)
                //{
                //    i = 0;
                //}
                //listMax = listMax - 1;
                
            %>           
          <tr>
              <th rowspan="2"><img src="<%=urlList.get(i)%>" height="42" width="42"></th>
            <th colspan="3" rowspan="2"><a href=<%="auctionShowAuction.htm?id_auction="+auction.get(i).getId_auction()%>> <%=auction.get(i).getTitle()%></a></th>
            <th><%=auction.get(i).getPrice()%></th>
          </tr>
          <tr>
            <td><%=auction.get(i).getExpirationDate()%></td>
          </tr>
            <%     
            }
            %>
            </table><%
        %>
        <%-- Lapanie wyjatkow i zamykanie polaczen --%>   
        <%        }
        catch (Exception ex) {
        %><%=ex%><%
                        ex.printStackTrace();
                    }
                    finally {
                        try {
                            if (rs != null) {
                                rs.close();
                                rs = null;
                            }
                            if (ps != null) {
                                ps.close();
                                ps = null;
                            }
                            if (con != null) {
                                con.close();
                                con = null;
                            }
                        }
                        catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
        %>
    </body>
</html>
