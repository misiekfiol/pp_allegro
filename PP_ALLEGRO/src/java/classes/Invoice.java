/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import classes.Users.Address;
import classes.Product.Auction;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Cascade;

/**
 *
 * @author Michał Tworuszka
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "INVOICE")
public class Invoice implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "ID_INVOICE")
    private int id_invoice;
    @Column(name = "companyName")
    private String companyName;
    @Column(name = "NIP")
    private int NIP;
    @Column(name = "address_id")
    private String address_id;

    public Invoice() {

    }
    
    public Invoice(String companyName, int NIP, String address_id)
    {
        this.companyName = companyName;
        this.NIP = NIP;
        this.address_id = address_id;
    }

    public Invoice(int id_invoice, String companyName, int NIP, String address_id)
    {
        this.id_invoice = id_invoice;
        this.companyName = companyName;
        this.NIP = NIP;
        this.address_id = address_id;
    }

    public int getId_invoice()
    {
        return id_invoice;
    }

    public String getCompanyName()
    {
        return companyName;
    }

    public int getNIP()
    {
        return NIP;
    }

    public String getAddress_id()
    {
        return address_id;
    }

    public void setId_invoice(int id_invoice)
    {
        this.id_invoice = id_invoice;
    }

    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

    public void setNIP(int NIP)
    {
        this.NIP = NIP;
    }

    public void setAddress_id(String address_id)
    {
        this.address_id = address_id;
    }
    
    

}
