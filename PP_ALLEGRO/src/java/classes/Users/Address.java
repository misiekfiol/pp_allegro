/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes.Users;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

/**
 *
 * @author Michał Tworuszka
 */
@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@Table(name="ADDRESS")
public class Address implements Serializable {
    
    @Id
    @GeneratedValue
    @Column(unique = true, name = "ID_ADDRESS")
    private int id_address;
    @Column(name = "COUNTRY")
    private String country;
    @Column(name = "CITY")
    private String city;
    @Column(name = "POSTAL_CODE")
    private String postalCode;
    @Column(name = "STREET")
    private String street;
    @Column(name = "HOUSE")
    private String house;
    @Column(name = "FLAT")
    private String flat;

    public Address() {
    }
    
    public Address(String country, String city, String postalCode, String street, String house, String flat) {
        this.country = country;
        this.city = city;
        this.postalCode = postalCode;
        this.street = street;
        this.house = house;
        this.flat = flat;
    }

    public Address(int id_address, String country, String city, String postalCode, String street, String house, String flat) {
        this.id_address = id_address;
        this.country = country;
        this.city = city;
        this.postalCode = postalCode;
        this.street = street;
        this.house = house;
        this.flat = flat;
    }
    

    public int getId_address() {
        return id_address;
    }

    public void setId_address(int id_address) {
        this.id_address = id_address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getFlat() {
        return flat;
    }

    public void setFlat(String flat) {
        this.flat = flat;
    }      
}
