/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes.Product;

import classes.Users.Address;
import java.io.Serializable;
import java.sql.Blob;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.Cascade;
/**
 *
 * @author Michał Tworuszka
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "AUCTION")
public class Auction implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "ID_AUCTION")
    private int id_auction;
    @Column(name = "TITLE")
    private String title;
    @Column(name = "PRICE")
    private double price;
    @Column(name = "DESCRYPTION")
    private String descryption;
    @Column(name = "expirationDate")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date expirationDate;
    @Column(name = "PHOTO")
    private Blob photo;
    @Column(name = "address_id")
    private int address_id;
    @Column(name = "end_id")
    private int end_id;
    @Column(name = "owner_id")
    private int owner_id;

    public Auction() {

    }

    public Auction(int id_auction, String title, double price, String descryption, Date expirationDate, Blob photo, int address_id, int end_id,int owner_id)
    {
        this.id_auction = id_auction;
        this.title = title;
        this.price = price;
        this.descryption = descryption;
        this.expirationDate = expirationDate;
        this.photo = photo;
        this.address_id = address_id;
        this.end_id = end_id;
        this.owner_id = owner_id;
    }

    public Auction(String title, double price, String descryption, Date expirationDate, Blob photo, int address_id, int end_id, int owner_id)
    {
        this.title = title;
        this.price = price;
        this.descryption = descryption;
        this.expirationDate = expirationDate;
        this.photo = photo;
        this.address_id = address_id;
        this.end_id = end_id;
        this.owner_id = owner_id;
    }

    public int getId_auction()
    {
        return id_auction;
    }

    public String getTitle()
    {
        return title;
    }

    public double getPrice()
    {
        return price;
    }

    public String getDescryption()
    {
        return descryption;
    }

    public Date getExpirationDate()
    {
        return expirationDate;
    }

    public Blob getPhoto()
    {
        return photo;
    }

    public int getAddress_id()
    {
        return address_id;
    }

    public int getEnd_id()
    {
        return end_id;
    }

    public int getOwner_id()
    {
        return owner_id;
    }

    public void setId_auction(int id_auction)
    {
        this.id_auction = id_auction;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public void setPrice(double price)
    {
        this.price = price;
    }

    public void setDescryption(String descryption)
    {
        this.descryption = descryption;
    }

    public void setExpirationDate(Date expirationDate)
    {
        this.expirationDate = expirationDate;
    }

    public void setPhoto(Blob photo)
    {
        this.photo = photo;
    }

    public void setAddress_id(int address_id)
    {
        this.address_id = address_id;
    }

    public void setEnd_id(int end_id)
    {
        this.end_id = end_id;
    }


    public void setOwner_id(int owner_id)
    {
        this.owner_id = owner_id;
    }


    
}
