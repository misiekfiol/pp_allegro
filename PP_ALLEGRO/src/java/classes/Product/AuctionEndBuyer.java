/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes.Product;

import classes.Users.Address;
import classes.Users.User;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Cascade;

/**
 * Information fo seller an mail for seller
 *
 * @author Michał Tworuszka
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "AUCTION_EB")
public class AuctionEndBuyer implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "id_ofEndBuyer")
    private int id_ofEndBuyer;
    @Column(name = "AUCTION_ID")
    private int auction_id;
    @Column(name = "PRICE")
    private double price;
    @Column(name = "bankNumber")
    private String bankNumber;
    @Column(name = "DESCRYPTION")
    private String descryption;
    @Column(name="SELLER_ID")
    private int seller_id;

    public AuctionEndBuyer() {

    }

    public AuctionEndBuyer(int auction_id, double price, String bankNumber, String descryption, int seller_id) {
        this.auction_id = auction_id;
        this.price = price;
        this.bankNumber = bankNumber;
        this.descryption = descryption;
        this.seller_id = seller_id;
    }

    public AuctionEndBuyer(int id_ofEndBuyer, int auction_id, double price, String bankNumber, String descryption, int seller_id) {
        this.id_ofEndBuyer = id_ofEndBuyer;
        this.auction_id = auction_id;
        this.price = price;
        this.bankNumber = bankNumber;
        this.descryption = descryption;
        this.seller_id = seller_id;
    }

    public int getId_ofEndBuyer() {
        return id_ofEndBuyer;
    }

    public void setId_ofEndBuyer(int id_ofEndBuyer) {
        this.id_ofEndBuyer = id_ofEndBuyer;
    }

    public int getAuction_id() {
        return auction_id;
    }

    public void setAuction_id(int auction_id) {
        this.auction_id = auction_id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getBankNumber() {
        return bankNumber;
    }

    public void setBankNumber(String bankNumber) {
        this.bankNumber = bankNumber;
    }

    public String getDescryption() {
        return descryption;
    }

    public void setDescryption(String descryption) {
        this.descryption = descryption;
    }

    public int getSeller_id() {
        return seller_id;
    }

    public void setSeller_id(int seller_id) {
        this.seller_id = seller_id;
    }

    
}
