/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes.Product;

import classes.Invoice;
import classes.Users.*;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Cascade;

/**
 *
 * @author Michał Tworuszka
 */
@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@Table(name="AUCTION_ES")
public class AuctionEndSeller implements Serializable  {
    
    @Id
    @GeneratedValue
    @Column(unique = true, name = "id_ofEndSell")
    private int id_ofEndSell;
    @Column(name = "auctionID")
    private int auctionID;
    @Column(name="ADDRESS_ID")
    private int address_id;
    @Column(name="DELIVERY_ID")
    private int delivery_id;
    @Column(name = "isInvoice")
    private boolean isInvoice;
    @Column(name="BUYER_ID")
    private int buyer_id;
    @Column(name="INVOICE_ID")
    private int invoice_id;

    public AuctionEndSeller() {
    }

    public AuctionEndSeller(int auctionID, int address_id, int delivery_id, boolean isInvoice, int buyer_id, int invoice_id) {
        this.auctionID = auctionID;
        this.address_id = address_id;
        this.delivery_id = delivery_id;
        this.isInvoice = isInvoice;
        this.buyer_id = buyer_id;
        this.invoice_id = invoice_id;
    }

    public AuctionEndSeller(int id_ofEndSell, int auctionID, int address_id, int delivery_id, boolean isInvoice, int buyer_id, int invoice_id) {
        this.id_ofEndSell = id_ofEndSell;
        this.auctionID = auctionID;
        this.address_id = address_id;
        this.delivery_id = delivery_id;
        this.isInvoice = isInvoice;
        this.buyer_id = buyer_id;
        this.invoice_id = invoice_id;
    }

    public int getId_ofEndSell() {
        return id_ofEndSell;
    }

    public void setId_ofEndSell(int id_ofEndSell) {
        this.id_ofEndSell = id_ofEndSell;
    }

    public int getAuctionID() {
        return auctionID;
    }

    public void setAuctionID(int auctionID) {
        this.auctionID = auctionID;
    }

    public int getAddress_id() {
        return address_id;
    }

    public void setAddress_id(int address_id) {
        this.address_id = address_id;
    }

    public int getDelivery_id() {
        return delivery_id;
    }

    public void setDelivery_id(int delivery_id) {
        this.delivery_id = delivery_id;
    }

    public boolean isIsInvoice() {
        return isInvoice;
    }

    public void setIsInvoice(boolean isInvoice) {
        this.isInvoice = isInvoice;
    }

    public int getBuyer_id() {
        return buyer_id;
    }

    public void setBuyer_id(int buyer_id) {
        this.buyer_id = buyer_id;
    }

    public int getInvoice_id() {
        return invoice_id;
    }

    public void setInvoice_id(int invoice_id) {
        this.invoice_id = invoice_id;
    }

}
