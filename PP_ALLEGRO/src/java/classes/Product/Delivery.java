/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes.Product;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

/**
 *
 * @author Michał Tworuszka
 */
@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@Table(name="DELIVERY")
public class Delivery implements Serializable  {
    
    @Id
    @GeneratedValue
    @Column(unique = true, name = "ID_DELIVERY")
    private int id_delivery;
    @Column(name = "typeOfDelivery")
    private String typeOfDelivery;
    @Column(name = "timeOD")
    private int timeOD;
    @Column(name = "PRIVE")
    private double price;

    public Delivery() {
    }

    public Delivery(String typeOfDelivery, int timeOD, double price) {
        this.typeOfDelivery = typeOfDelivery;
        this.timeOD = timeOD;
        this.price = price;
    }

    public int getId_delivery() {
        return id_delivery;
    }

    public void setId_delivery(int id_delivery) {
        this.id_delivery = id_delivery;
    }

    public String getTypeOfDelivery() {
        return typeOfDelivery;
    }

    public void setTypeOfDelivery(String typeOfDelivery) {
        this.typeOfDelivery = typeOfDelivery;
    }

    public int getTimeOD() {
        return timeOD;
    }

    public void setTimeOD(int timeOD) {
        this.timeOD = timeOD;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    
}
