/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hiberApp;

import classes.Product.*;
import classes.Invoice;
import classes.Users.*;
import java.io.File;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 *
 * @author Michał Tworuszka
 */
public final class HiberUtil {

    public enum Mapping {

        XML, ANN;
    }

    public static SessionFactory getSessionFactory(Mapping mapping) {

        switch (mapping) {
            case XML:
                return (getXMLSessionFactory());
            case ANN:
                return (getANNSessionFactory());
            default:
                return (getANNSessionFactory());
        }
    }

    public static SessionFactory getXMLSessionFactory() {
        try {
            File mappingDir = new File("src\\mapowanie");
            Configuration config = new Configuration().configure();

            config.setProperty("hibernate.show_sql", "false");
            config.addDirectory(mappingDir);

            StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
            registryBuilder.applySettings(config.getProperties());
            ServiceRegistry serviceRegistry = registryBuilder.build();

            SessionFactory sf = config.buildSessionFactory(serviceRegistry);

            return (sf);
        }
        catch (Throwable ex) {

            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getANNSessionFactory() {
        try {
            Configuration config = new Configuration().configure();
            config.setProperty("hibernate.show_sql", "false");

            config.addAnnotatedClass(Address.class)
                    .addAnnotatedClass(User.class)
                    .addAnnotatedClass(Delivery.class)
                    .addAnnotatedClass(Auction.class)
                    .addAnnotatedClass(AuctionEndBuyer.class)
                    .addAnnotatedClass(AuctionEndSeller.class)
                    .addAnnotatedClass(Invoice.class);

            config.setProperty("hibernate.show_sql", "false");

            StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
            registryBuilder.applySettings(config.getProperties());
            ServiceRegistry serviceRegistry = registryBuilder.build();

            SessionFactory sf = config.buildSessionFactory(serviceRegistry);

            return (sf);
        }
        catch (Throwable ex) {

            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
}