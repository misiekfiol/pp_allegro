/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Mikołaj Sikorski
 */
public class ImageServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        java.sql.Connection sqlConnection = null;
        java.sql.Statement sqlStatement = null;
        java.sql.ResultSet sqlResultSet = null;
        java.sql.PreparedStatement sqlPreparedStatement = null;

        String sqlUrl = "jdbc:derby://localhost:1527/BazaDanych";
        String sqlShema = "baza";
        String sqlUser = "baza";

        int cust_id = Integer.parseInt(request.getParameter("id"));
        // String sqlString = "select * from tbl_sys_user";
        //sqlStatement = sqlConnection.createStatement();
        //sqlResultSet = sqlStatement.executeQuery(sqlString);
        ServletOutputStream out = response.getOutputStream();
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver");
            sqlConnection = java.sql.DriverManager.getConnection(sqlUrl, sqlShema, sqlUser);
            out.println("Prubuje cos wyswietlic");
            sqlPreparedStatement = sqlConnection.prepareStatement("Select img from tbl_sys_user where cust_id = ?");
            sqlPreparedStatement.setInt(1, cust_id);
            sqlResultSet = sqlPreparedStatement.executeQuery();
            if (sqlResultSet.next()) {
                java.sql.Blob blob = sqlResultSet.getBlob("img");
                byte[] image = blob.getBytes(1, (int) blob.length());
                OutputStream outputStream = response.getOutputStream();
                outputStream.write(image);
            }
            else {
                response.setContentType("TEXT/HTML");
                out.println("<html><head><title>Display out the blob image</title></head>");
                out.println("<body><h4><font color = 'red'>Image not founde for the given id</font></h4></body></html>");
                return;
            }

        }
        catch (Exception e) {
            response.setContentType("TEXT/HTML");
            out.println("<html><head><title>Unable To Display image</title></head>");
            out.println("<body><h4><font color='red'>Image Display Error="
                    + e.getMessage() + "</font></h4></body></html>");
            return;

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
